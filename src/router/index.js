import { createBrowserRouter } from "react-router-dom";
import Home from "../pages/home";
import Detail from "../pages/detail";
import Pencarian from "../pages/pencarian";

export const router = createBrowserRouter([
    {
        path: '/',
        element: <Home />
    },
    {
        path: '/detail/:imdbID',
        element: <Detail />
    },
    {
        path: '/pencarian/:Title',
        element: <Pencarian />
    }
])