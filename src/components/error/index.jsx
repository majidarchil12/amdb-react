export default function Error({ title }) {
    return (
        <>
            <h1 className="text-info">Pencarian : <b>{title}</b></h1>
            <div className="d-flex align-items-center justify-content-center vh-100">
                <h1 className="text-danger">Judul tidak ditemukan</h1>
            </div>
        </>
    )
}