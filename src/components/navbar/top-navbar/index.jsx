import { Container, Nav, Navbar } from "react-bootstrap";
import SearchForm from "../../search-form";
import { Link } from "react-router-dom";

export default function TopNavbar() {
    return (
        <>
            <Navbar collapseOnSelect expand="lg" className="bg-body-tertiary">
                <Container>
                    <Link to={'/'} className="navbar-brand">
                        AMDB
                    </Link>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="ms-auto">
                            <SearchForm />
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
}