import useSWR from "swr";
import LoadingSpinner from "../../spinner";
import { Button, Card, Col, Image, ListGroup, Row } from "react-bootstrap";
import { Award, Star } from "react-bootstrap-icons";
import Header from "../../header";

export default function MovieDetail({ imdbID }) {
    const omdbApiKey = process.env.REACT_APP_OMDB_API_KEY
    const api = `https://www.omdbapi.com/?i=${imdbID}&apikey=${omdbApiKey}&plot=full`

    const fetcher = (url) => fetch(url).then((res) => res.json());

    const { data, error, isLoading } = useSWR(
        api,
        fetcher
    )

    if (error) return "An error has occurred."
    if (isLoading) return <LoadingSpinner />

    return (
        <>
            <Header title={data.Title} />
            <Row className="mt-3 border">
                <Col className="my-2 text-center">
                    <Image className="h-100 d-inline-block" src={data.Poster} />
                </Col>
                <Col className="my-2">
                    <h3>
                        {data.Title} ({data.Year}) <br />
                    </h3>
                    <h5>
                        <Award color="gold" /> {data.Awards}
                    </h5>
                    <p>
                        <Button variant="outline-info" size="sm" className="rounded-pill">
                            {data.Genre}
                        </Button>
                    </p>
                    <hr />
                    <p className="text-break">
                        {data.Plot}
                    </p>
                    <hr />
                    <p>Rate : {data.Rated}</p>
                    <hr />
                    <p>Director : {data.Director} | Writer : {data.Writer} </p>
                    <hr />
                    <p>Actor : {data.Actors}</p>
                    <hr />
                    <Card className="mb-2">
                        <ListGroup variant="flush">
                            {
                                data.Ratings.map((rating, index) => {
                                    return (
                                        <ListGroup.Item>
                                            <Star color="gold" className="me-2" /> {rating.Source} - {rating.Value}
                                        </ListGroup.Item>
                                    )
                                })
                            }
                        </ListGroup>
                    </Card>
                </Col>
            </Row>

        </>
    )
}