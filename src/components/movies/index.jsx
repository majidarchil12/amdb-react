import { useState } from "react";
import { Card, Col, Row } from "react-bootstrap";
import useSWR from "swr";
import LoadingSpinner from "../spinner";
import { Link } from "react-router-dom";

export default function Movies({ children }) {

    const [currentPage, setCurrentPage] = useState(1)
    const omdbApiKey = process.env.REACT_APP_OMDB_API_KEY
    const api = `https://www.omdbapi.com/?s=sea&apikey=${omdbApiKey}&page=${currentPage}`

    const imageWidth = {
        width: '192px'
    }

    const fetcher = (url) => fetch(url).then((res) => res.json());

    const { data, error, isLoading } = useSWR(
        api,
        fetcher
    )

    if (error) return "An error has occurred."
    if (isLoading) return <LoadingSpinner />

    return (
        <Row className="mt-5">
            {
                data.Search.map((data, index) => {
                    return (
                        <Col key={data.imdbID} md={4} className="mb-3" >
                            <Card>
                                <center>
                                    <Link to={`/detail/${data.imdbID}`}>
                                        <Card.Img variant="top" className="mt-2 px-2" alt="movie image" src={data.Poster} />
                                    </Link>
                                </center>
                                <Card.Body>
                                    <Link to={`/detail/${data.imdbID}`}>
                                        <Card.Title>{data.Title}</Card.Title>
                                    </Link>
                                    <Card.Text>
                                        Year {data.Year} <br />
                                        Type {data.Type}
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    )
                })
            }
        </Row>
    )

}