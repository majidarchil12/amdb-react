import { Container } from "react-bootstrap";
import TopNavbar from "../navbar/top-navbar";
import Header from "../header";

export default function Template({ children, className }) {
    return (
        <>
            <Header />
            <TopNavbar />
            <Container className={className}>
                {children}
            </Container>
        </>
    )
}