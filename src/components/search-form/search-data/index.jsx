import { Card, Col, Row } from "react-bootstrap";
import LoadingSpinner from "../../spinner";
import useSWR from "swr";
import { Link } from "react-router-dom";
import Header from "../../header";
import Error from "../../error";

export default function SearchData({ Title }) {

    const omdbApiKey = process.env.REACT_APP_OMDB_API_KEY
    const api = `https://www.omdbapi.com/?s=${Title}&apikey=${omdbApiKey}`

    const fetcher = (url) => fetch(url).then((res) => res.json());

    const { data, error, isLoading } = useSWR(
        api,
        fetcher
    )

    if (error) return "An error has occurred."
    if (isLoading) return <LoadingSpinner />

    if (data.Response === 'False') {
        return (
            <>
                <Header title={`Pencarian - ${Title}`} />
                <Error title={Title} />
            </>
        )
    } else {
        return (
            <>
                <Header title={`Pencarian - ${Title}`} />
                <Row className="mt-5">

                    <h1 className="text-info">Pencarian : <b>{Title}</b></h1>

                    {
                        data.Search.map((data, index) => {
                            return (
                                <Col key={data.imdbID} md={4} className="mb-3" >
                                    <Card>
                                        <center>
                                            <Link to={`/detail/${data.imdbID}`}>
                                                <Card.Img variant="top" className="mt-2 px-2" alt={`${data.Title} image`} src={data.Poster} />
                                            </Link>
                                        </center>
                                        <Card.Body>
                                            <Link to={`/detail/${data.imdbID}`}>
                                                <Card.Title>{data.Title}</Card.Title>
                                            </Link>
                                            <Card.Text>
                                                Year {data.Year} <br />
                                                Type {data.Type}
                                            </Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )
                        })
                    }
                </Row>
            </>
        )
    }
}