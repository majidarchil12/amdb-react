import { Button, Col, Form, Row } from "react-bootstrap";
import { Search } from "react-bootstrap-icons";
import { useNavigate } from "react-router-dom";

export default function SearchForm() {
    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault()

        const Title = e.target.elements.search.value

        navigate(`/pencarian/${Title}`)
    }

    return (
        <>
            <Form onSubmit={handleSubmit}>
                <Row>
                    <Col xs="auto">
                        <Form.Control
                            type="text"
                            placeholder="Cari Judul"
                            className=" mr-sm-2" name="search" minLength={3}
                        />
                    </Col>
                    <Col xs="auto">
                        <Button type="submit">
                            <Search />
                        </Button>
                    </Col>
                </Row>
            </Form>
        </>
    )
}