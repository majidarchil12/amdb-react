export default function Header({ title }) {

    if (title === undefined) {
        title = 'AMDB'
    } else {
        title = 'AMDB | ' + title
    }

    document.title = title
}