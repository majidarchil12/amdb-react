import { useParams } from "react-router-dom";
import SearchData from "../../components/search-form/search-data";
import Template from "../../components/templates";

export default function Pencarian() {
    const { Title } = useParams();
    return (
        <>
            <Template>
                <SearchData Title={Title} />
            </Template>
        </>
    )
}