import Movies from "../../components/movies";
import Template from "../../components/templates";

export default function Home() {
    return (
        <>
            <Template>
                <Movies />
            </Template>
        </>
    )
}