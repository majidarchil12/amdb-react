import MovieDetail from "../../components/movies/details";
import Template from "../../components/templates";
import { useParams } from 'react-router-dom';

export default function Detail() {

    const { imdbID } = useParams();

    return (
        <>
            <Template>
                <MovieDetail imdbID={imdbID} />
            </Template>
        </>
    )
}